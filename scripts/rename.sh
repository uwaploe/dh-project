#!/usr/bin/env bash
#
# Rename Podman images by changing the docker.io reference to localhost
#

while read imname imid; do
    name="${imname/docker.io/localhost}"
    podman tag $imid "$name"
    podman rmi "$imname"
done < <(podman images --filter 'reference=docker.io/*' --format '{{.Repository}}:{{.Tag}} {{.ID}}')
