#!/usr/bin/env bash
#

op="$1"
[[ -z $op ]] && {
    echo "Usage: $(basename $0) start|stop|restart"
    exit 1
}

services=(
    must-container-ntrip.service
    pod-must-broker.service
    must-navsvc.service
    must-dvlsetup.service
    must-nmeapub.service
    must-nmeadist@navsys.service
    must-rdrovins.service
    must-ins2ixblue.service
    must-rovinsaiding.service
    must-inspub.service
    must-svspub.service
    must-hpdata.service)

for svc in "${services[@]}"; do
    echo "systemctl --user $op $svc"
    systemctl --user $op $svc
    sleep 0.2
done
