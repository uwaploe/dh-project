#!/usr/bin/env bash
#
# Start all MuST datahub services
#

services=(
    must-container-ntrip.service
    pod-must-broker.service
    must-navsvc.service
    must-nmeapub.service
    must-nmeadist@navsys.service
    must-rdrovins.service
    must-ins2ixblue.service
    must-rovinsaiding.service
    must-inspub.service
    must-svspub.service
    must-hpdata.service)

for svc in "${services[@]}"; do
    echo "systemctl --user start $svc"
    systemctl --user start $svc
    sleep 0.2
done
