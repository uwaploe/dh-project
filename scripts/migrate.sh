#!/usr/bin/env bash
#
# Migrate Docker images to Podman
#

for img in $(docker images --format '{{.Repository}}:{{.Tag}}'); do
    echo $img
    podman pull docker-daemon:$img
done
