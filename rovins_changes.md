# Data-Hub changes to support iXBlue Rovins

## Navigation Data

The format of the navigation data provided by the Data-Hub has not changed but the DVL data is no longer logged as the DVL is now directly connected to the Rovins INS. The DVL data will be available in the Rovins data stored in the Data-Hub archive as well as in the logs stored on the Rovins itself.

## Aiding Data

GNSS data for the Rovins is extracted from the Inertial Labs INS data stream and passed to the Rovins on TCP port 8177. Depth and sound velocity values are passed on TCP port 8122.

## Archived Data

The Data-Hub will archive the real-time navigation data records sent by the Rovins. The records will be archived in their "wire format", iXBlue Std Binary V3, as documented in the *Rovins Nano Interface Library* document provided by iXBlue.
