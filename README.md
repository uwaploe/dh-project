# MUST Data-hub Services

This repository contains the [Systemd](https://www.freedesktop.org/wiki/Software/systemd/) service files that manage the applications of the MuST Data-hub. The services run under the *sysop* user account on the data-hub computer.


## Dataflow Diagram

![Dataflow](datahub.svg "data-flow diagram")

## Services

| **Name**     | **Description**                                  |
|--------------|--------------------------------------------------|
| nats         | Message Broker                                   |
| inspub       | Inertial Labs GPS/INS monitor and control        |
| dvlsetup     | DVL initialization                               |
| svspub       | Sound Velocity Sensor monitor                    |
| navsvc       | Navigation message server                        |
| nmeapub      | NMEA message publisher                           |
| nmeadist     | NMEA message distributer                         |
| archiver     | Data archiver                                    |
| ntrip        | NTRIP client                                     |
| rdrovins     | Publish iXBlue INS data                          |
| ins2ixblue   | Send GPS data to iXBlue INS                      |
| rovinsaiding | Send depth and sound velocity data to iXBlue INS |
| hpdata       | Provides additional navigation data to HyPack    |
