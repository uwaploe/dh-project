#!/usr/bin/env bash
#
# Create systemd service files for Podman pods.
#
POD="must-broker"
NATS_VERS="latest"
ARCHIVER_VERS="latest"

source ../config/nats.env

systemctl --user stop pod-${POD}.service

podman pod rm -f $POD
podman pod create --name $POD -p 8222:8222 -p 4222:4222
podman create --pod $POD \
       --name nats \
       --expose 4222 --expose 8222 \
       --log-driver=journald \
       docker.io/library/nats-streaming:${NATS_VERS} \
       -m 8222 -cid $NATS_CLUSTER_ID -mc 100 -mb 20MB
podman create --pod $POD \
       --name archiver \
       --env "NATS_CLUSTER_ID=${NATS_CLUSTER_ID}" \
       --env "NATS_URL=nats://localhost:4222" \
       --log-driver=journald \
       -v $HOME/config:/config:ro \
       -v $HOME/data/MUST:/data \
       localhost/must/natsarchiver:${ARCHIVER_VERS} /config/archive.toml

(
    cd $HOME/.config/systemd/user
    podman generate systemd --files --name $POD
)

systemctl --user daemon-reload
