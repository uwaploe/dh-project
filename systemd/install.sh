#!/usr/bin/env bash

SVCDIR="$HOME/.config/systemd/user"

mkdir -p "$SVCDIR"
cp -v *.service "$SVCDIR"

dirs=(*.service.d)
for d in "${dirs[@]}"; do
    cp -av "$d" "$SVCDIR"
done

systemctl --user daemon-reload
