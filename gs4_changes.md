# Data-Hub changes to support GS4

## Navigation Data

The navigation solution data provided by the GS4 is a very close match to the Atlas data format that we currently provided to the EBoss and Tim Marston's processing software (Atlas and GS4 use the same axis conventions). The format of the navigation data provided by the Data-Hub won't change but we will leave the following data fields empty:

- DVL range
- Magnetometer

## Aiding Data

GPS data for the GS4 will be extracted from the Intertial Labs INS and passed to the GS4 on UDP port 5001 as NMEA sentences. The DVL PD4 data records will be passed to the GS4 on UDP port 5000.

## Archived Data

The Data-Hub will archive a number of the "data streams" provided by the GS4. The GS4 uses [LCM](http://lcm-proj.github.io) to publish all data. These data records will be archived using the [LCM Log](http://lcm-proj.github.io/log_file_format.html) format. Consult the previous links for a detailed description of the file format.

The record types listed below will be archived.

| **Channel**               | **Data Type**   | **Description**                   |
|---------------------------|-----------------|-----------------------------------|
| OPENINS_NAV_SOLUTION      | nav_solution_t  | Calculated navigation solution    |
| OPENINS_IMU_STAT          | imu_stat_t      | Data from the GS4 IMU             |
| OPENINS_PRESSURE_STAT     | pressure_stat_t | Data from the GS4 pressure sensor |
| OPENINS_NAV_CMD           | pcomms_t        | Commands sent to the GS4          |
| OPENINS_GPS_STAT_1*       | gps_data_t      | GS4 GPS aiding data               |
| OPENINS_GPS_COMPASS_STAT* | compass_stat_t  | GS4 heading aiding data           |
| OPENINS_DVL_STAT*         | dvl_stat_t      | GS4 DVL aiding data               |

*These records are used to confirm that the GS4 is receiving the aiding data and might eventually be dropped from the archive.
